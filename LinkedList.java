import java.util.*;
class LinkedList
{
  static Node head;
  static class Node
  {
	  int data;
	  Node next;
	  Node(int data)
	  {
		  this.data=data;
		  next=null;
	  }
  }
  void InsertNode(int new_data)
  {   
	  Node n1= new Node(new_data);
	  n1.next=head;
	  head=n1;
  }
  
  Node ReverseDisplay(Node node)
  {
	    Node prev =null;
		Node Curr =node;
		Node next=null;
		while(Curr !=null)
		{
			next=Curr.next;
			Curr.next=prev;
			prev=Curr;
			Curr=next;
		}
		node=prev;
		return node;
				
  }
  void Print(Node node)
  {
	  while(node!=null)
	  {
		  System.out.print(node.data +" ");
		  node=node.next;
	  }
  }
  
  public static void main(String args[])
  {   
	  LinkedList l1=new LinkedList();
	  l1.head=new Node(3);
	  l1.head.next=new Node(4);
	  l1.head.next.next=new Node(2);
	  l1.head.next.next.next=new Node(5);
	  //l1.head.next.next.next.next=new Node(5);
	  head=l1.ReverseDisplay(head);
	  l1.Print(head);
  }
}