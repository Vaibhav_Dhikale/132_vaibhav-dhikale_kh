class Stack
{
	int s;
	int top1;
	int top2;
	int arr[];
	
	Stack(int n)
	{
		arr=new int[n];
		s=n;
		top1=-1;
		top2=s;		
	}	
	void push1(int a)
	{
		if(top1 < top2-1)
		{
			top1++;
			arr[top1]=a;
		}
		else
		{
			System.out.println("Stsck Overflow");
			return;
		}
	}	
	void push2(int a)
	{
		if(top1< top2-1)
		{
			top2--;
			arr[top2]=a;			
		}
		else
		{
			System.out.println("Stsck Overflow");
			return;
		}
	}	
	void pop1()
	{
		if(top1>=0)
		{
			int a=arr[top1];
			top1--;
			System.out.println("Popped element from stack1 is "+a);
		}
	}	
	void pop2()
	{
		if(top2<s)
		{
			int a=arr[top2];
			top2++;
			System.out.println("Popped element from stack2 is "+a);
		}
	}
	
	public static void main(String args[])
	{
		Stack s1=new Stack(5);
		s1.push1(5);
		s1.push2(10);
		s1.push2(15);
		s1.push1(11);
		s1.pop1();
		s1.push2(7);
		s1.push2(40);
		s1.pop2();
		
		
		
	}
}